# Console Appearance

`vidcontrol`

`vidfont`

`pkg install x11-fonts/spleen`

To set a 1600x900 mode on all output connectors, put the following line in `/boot/loader.conf`

`kern.vt.fb.default_mode="1600x900"`

To set a 800x600 only on a laptop builtin screen, use the following line instead:

`kern.vt.fb.modes.LVDS-1="800x600"`

To set black and white colors of console palette

```sh
kern.vt.color.0.rgb="10,10,10"
kern.vt.color.15.rgb="#00ffff"
```

Show available colors

`vidcontrol show`

Set foreground and background colors

`vidcontrol white black`

set history = 1000

set savehist = 1000

`visudo` `alice ALL=(ALL) NOPASSWD: ALL`


