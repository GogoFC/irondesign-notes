![logo](images/duplicati.svg)

---

# Using [Duplicati](https://www.duplicati.com/) to backup data


Duplicati has a GUI which makes setting up easy.

# Install

[Download](https://www.duplicati.com/download) Duplicati.\


## Configure a new backup


### 1. Add new backup.

![add new](images/duplicatiaddnew.png)

### 2. Choose password.

![pass](images/duplicatipassword.png)

### 3. Enter server info.

- SFTP as storage type.
- Server FQDN or IP address and Port 22.
- Enter home directory as path on Server. `/home/{user}/`
- User and password.


![server](images/duplicatiserverinfo.png)

### 4. Hit yes to trust the host key. 

![trust](images/duplicatitrust.png)

### 5. Click Yes to create a new folder.

![folder](images/duplicatinewfolder.png)


### 6. Add data source.

![source](images/duplicatisource.png)


### 7. Schedule backups. 

![schedule](images/duplicatiischedule.png)

### 8. Save settings.

![save settings](images/duplicatisavesettings.png)

### 9. Save the Password. 

![save pwd](images/duplicatisavepassword.png)

### 10. You can click run now to run the backup immediately ahead of schedule. 

![run](images/duplicatirunnow.png)


# Restore files

### 1. Choose a backup to restore.

![restore](images/duplicatirestore.png)

### 2. You can choose to restore different versions of backups. 

![restore](images/duplicatirestore2.png)

### 3. Choose restore destination.

![restore](images/duplicatirestore3.png)
