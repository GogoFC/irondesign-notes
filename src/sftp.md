# [SFTP - Secure File Transfer Protocol](https://www.openssh.com/)

---

# Using SFTP to transfer data.

### Connect to Server via SFTP

```sh
sftp user@extralayer.eu
```
![con](images/sftpconnect.png)

### List files on remote server

```sh
ls
```
![ls](images/sftpls.png)

### Dowloading a file

```
get filename.txt
```
![get](images/sftpget.png)

### Uploading a flie

```sh
put filename.md
```
![put](images/sftpput.png)

## Other commands

Preserve time-stamps, important for Photo Albums.
```sh
put -p directory
```
Download a directory and the contents.
```sh
get -r directory
```
Upload a directory and the contents.
```sh
put -r directory
```

Display remote working directory.
```sh
pwd
```
Print local working directory.
```sh
lpwd
```
Display a remote directory listing
```sh
ls
```
Display local directory listing
```sh
lls
```
Quit sftp
```sh
bye
```
![bye](images/sftpbye.png)


### Resuming transfer

Copy remote directory contents to local dir `media` with resume and recursive flags.
```sh
sftp -ar iron@irondesign.dev:/home/iron/video C:\Users\Iron\Media
```
#### Options:

 
> `-r` Recursively      copy entire directories when uploading and downloading.  Note that sftp does not follow symbolic links encountered in the tree traversal.


> `-a` Attempt to continue interrupted transfers rather than overwriting existing partial or complete copies of files. If the partial contents differ from those being transferred, then the resultant file is likely to be corrupt.


# Docs

[Manual Page - SFTP](https://man.openbsd.org/sftp.1)
