# ZFS Storage on XCP-ng


### Install ZFS

```sh
yum install zfs
```

### Load Kernel Modules

```sh
depmod -a
```
```sh
modprobe zfs
```

### Create zpool

```sh
zpool create -o ashift=12 blue mirror /dev/sda /dev/sdb
```

### Create Dataset

```sh
zfs create blue/zlocal
```

### List Host uuid

```sh
xe host-list
```

Or set the variable

```sh
HOST_UUID=$(xe host-list | grep uuid | awk '{print $5}')
```
### Create SR

```sh
xe sr-create \
host-uuid=$HOST_UUID \
name-label="Local ZFS" \
type=zfs \
device-config:location=/blue/zlocal
```



# Docs


[docs.doubleu.codes](https://docs.doubleu.codes/guides/xcp-ng/zfs-local-storage/)

[XCP-ng documentation](https://xcp-ng.org/docs/storage.html#zfs)
