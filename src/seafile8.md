# Seafile 8.0

For Seafile 8.0.x or newer versions on Ubuntu 18.04/20.04 (64bit) server

# Install

Script auto install

```sh
cd /root
wget --no-check-certificate https://raw.githubusercontent.com/haiwen/seafile-server-installer/master/seafile-8.0_ubuntu
bash seafile-8.0_ubuntu 8.0.0
```

[Github haiwen seafile-server-installer](https://github.com/haiwen/seafile-server-installer#installation)

---

# Backup 


### Main Server

Mysqldump databases are kept for 12 days, those older than 12 days are deleted. 
They are very small in size. 

Contents of `/root` directory.  
`seafile_db` `seahub_db` `ccnet_db`

![rootmain](images/rootmain.png)

Space used by databases in Kilobytes.

![dbspaceused](images/dbspaceused.png)

Backup Script is located at `/root/.backupscript/seafile-data-backup.sh`

![script](images/scriptfolder.png)


### Backup Server

Backup directory `/root/giadrive-backup`

![backupfolder](images/backupfolder.png)

Databases directory `/root/giadrive-backup/db`

![dbfolder](images/dbfolder.png)

Logs directory `/root/giadrive-backup/logs`

![logs](images/logsfolder.png)

Log Contents

![readlog](images/readlog.png)

Seafile directory `/root/giadrive-backup/data/seafile`

![seafile](images/seafilefolder.png)

Space used

![usedspace](images/spaceused.png)

### Cron

```sh
# Renew SSL automatically
15 2 1 * * certbot renew --post-hook 'systemctl restart nginx'

# Backup script daily at midnight

0 23 * * * /root/.backupscript/seafile-data-backup.sh
```

### Backup Directories

Cron runs scripts in user's home directory.  

Logs and database backup files are saved to `/root` directory of main server and backed up in `/root/giadrive-backup` directory of the backup server.  
Copy of data is backed up only on backup server.

---

# Restore

### 0. Install Seafile

On the new Server install a fresh Seafile.

### 1. Copy data from backup server.

Copy entire `giadrive-backup` folder from backup server to new server.

```SH
rsync -avHP --delete giadrive-backup root@new.server.dev:/root
```
Login to new server and replace `seaflie-data` and `seahub-data` folders with the ones we backed up. Do not replace anything else in the `/opt/seafile/` directory besides those two directories.

```sh
mv -rf /root/giadrive-backup/data/seafile/seafile-data /opt/seafile/seafile-data
```

```sh
mv -rf /root/giadrive-backup/data/seafile/seahub-data /opt/seafile/seahub-data
```

Restore proper permissions

```sh
chown seafile:seafile /opt/seafile/seafile-data/ /opt/seafile/seahub-data/
```

### 2. Restore the database.

Extract the `.gz`archive fr each database.

```sh
gzip -d ccnet-db.sql.2013-10-19-16-00-05.gz
```

Import the databases. 

```sh
mysql -u root ccnet_db < ccnet-db.sql.2013-10-19-16-00-05
mysql -u root seafile_db < seafile-db.sql.2013-10-19-16-00-20
mysql -u root seahub_db < seahub-db.sql.2013-10-19-16-01-05
```

### 3. Run Seafile Filesystem Check

Run file system check as `seafile` user.

Scripts are located in `cd /opt/seafile/seafile-server-latest` directory.

```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seaf-fsck.sh
```

> Since database and data are backed up separately, they may become a little inconsistent with each other. 
> To correct the potential inconsistency, run seaf-fsck tool to check data integrity on the new machine. 
> [See seaf-fsck documentation](https://manual.seafile.com/maintain/seafile_fsck/).


### 4. Restart Seafile

Restart Seafile and Seahub as `seafile` user.

```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seafile.sh restart
```
```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seahub.sh restart
```

### Seafile docs

- [Restore from backup](https://manual.seafile.com/maintain/backup_recovery/#restore-from-backup)

- [Seafile FSCK](https://manual.seafile.com/maintain/seafile_fsck/)

- [Seafile FSCK](https://www.mianshigee.com/tutorial/Seafile-en/maintain-seafile_fsck.md)

- [Backup using Rsync](https://seafile.readthedocs.io/en/latest/backup/backup-rsync/)

### Seafile FSCK

The seaf-fsck tool accepts the following arguments:

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh [--repair|-r] [--export|-E export_path] [repo_id_1 [repo_id_2 ...]]

### Repairing Corruption

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh --repair

Most of time you run the read-only integrity check first, to find out which libraries are corrupted. And then you repair specific libraries with the following command:

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh --repair [library-id1] [library-id2] ...

---

# Data Transfer Speed

### Recovery data transfer speed considerations. 

In the event where a data recovery is needed, it would take 4 hours 25 minutes to complete a transfer of 200 GB of data on a VPS at limited IO at about 80 Mbps. 



### Next scheduled backups

Time to complete the next scheduled backup will depend on daily data added. Predicting about 5 minutes each day . 


| Additional Data   | Time to complete  |
|--------|------------|
| 2 GB   |  3 Minutes |
| 5 GB   | 7 Minutes |
| 10 GB  | 14 Minutes |
| 100 GB |  2 Hours   |
| 200 GB | 5 Hours   |
|500 GB  | 11 Hours  |

---

# Backup script

`/root/.backupscript/seafile-data-backup.sh`

```sh
#!/bin/sh

# (1) set up all the mysqldump variables
FILE0=seafile_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE1=seahub_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE2=ccnet_db.`date +"%Y-%m-%d-%H-%M-%S"`
DBSERVER=localhost
DATABASE0=seafile_db
DATABASE1=seahub_db
DATABASE2=ccnet_db
USER=root

unalias rm     2> /dev/null


# use this command for a database server on a separate host:
#mysqldump --opt --protocol=TCP --user=${USER} --host=${DBSERVER} ${DATABASE} > ${FILE}


mysqldump --opt --user=${USER} ${DATABASE0} > ${FILE0}
mysqldump --opt --user=${USER} ${DATABASE1} > ${FILE1}
mysqldump --opt --user=${USER} ${DATABASE2} > ${FILE2}

# (4) gzip the mysql database dump file
gzip $FILE0
gzip $FILE1
gzip $FILE2

# rsync mysqldump
rsync -avHP --delete $FILE0.gz $FILE1.gz $FILE2.gz root@10.10.10.10:/root/giadrive-backup/db/ --log-file=seafile-backup-db.log

# Finds files ending in .gz older than 12 days and deletes them
find /root -mtime +12 -type f -name '*.gz' -delete

sleep 1

# rsync seafile data folder
rsync -avHP --delete /opt/seafile root@10.10.10.10:/root/giadrive-backup/data/ --log-file=seafile-backup-data.log

# rsync logs
rsync -avHP --delete *.log root@10.10.10.10:/root/giadrive-backup/logs/
```




