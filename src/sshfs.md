# [SSHFS](https://github.com/libfuse/sshfs)

---

#  Mount a remote file system using SSHFS.

# Linux

###  Install

```sh
sudo apt install sshfs
```

###  Mount

Mount a remote directory `disks` to `~/extra`.
```sh
sshfs user118@65.21.93.164:/home/user118/disks ~/extra
```
Unmount.
```sh
fusermount -u /home/ubuntu/extra
```
Check for mounted directories.
```sh
cat /etc/mtab | grep user118
```
![sshfs](images/sshfs.png)

# Windows

### Install

These are needed to run SSHFS on Windows.

[WinFsp](https://github.com/winfsp/winfsp)

[SSHFS-Win](https://github.com/winfsp/sshfs-win)

### 1. Map a Network Drive.

![0](images/sshfswin0.png)

### 2. Choose Drive Letter, Enter login info.

```sh
\\sshfs.k\sky3853@65.21.93.164
```

![0](images/sshfswin1.png)

### 3. Connected.

![0](images/sshfswin.png)

### 4. Right click Show more options.
![0](images/sshfswin-1.png)

### 5. Disconnect.

![0](images/sshfswin-2.png)
