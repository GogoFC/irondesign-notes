# Let's Encrypt

### Snap

```sh
sudo apt install snapd
```

```sh
sudo snap install core; sudo snap refresh core
```

```sh
sudo snap install --classic certbot
```
```sh
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```
```sh
sudo certbot --nginx
```
```sh
sudo certbot renew --dry-run
```

### python3-certbot-nginx 

```sh
sudo apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface
```


```sh
sudo apt install python3-certbot-nginx

```
```sh
sudo certbot --nginx
```

```sh
sudo certbot --nginx -d your_domain -d www.your_domain
```

```sh
sudo certbot renew --dry-run
```

### Delete SSL

```sh
sudo certbot delete
```
