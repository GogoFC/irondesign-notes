# PHP


Checking for installed php modules and packages

php -m

Checking for installed php modules and packages
In addition to running

php -m
to get the list of installed php modules, you will probably find it helpful to get the list of the currently installed php packages in Ubuntu:

sudo dpkg --get-selections | grep -v deinstall | grep php
This is helpful since Ubuntu makes php modules available via packages.

You can then install the needed modules by selecting from the available Ubuntu php packages, which you can view by running:

sudo apt-cache search php | grep "^php5-"
Or, for Ubuntu 16.04 and higher:

sudo apt-cache search php | grep "^php7"
As you have mentioned, there is plenty of information available on the actual installation of the packages that you might require, so I won't go into detail about that here.

Related: Enabling / disabling installed php modules
It is possible that an installed module has been disabled. In that case, it won't show up when running php -m, but it will show up in the list of installed Ubuntu packages.

Modules can be enabled/disabled via the php5enmod tool (phpenmod on later distros) which is part of the php-common package.

Ubuntu 12.04:

Enabled modules are symlinked in /etc/php5/conf.d

Ubuntu 12.04: (with PHP 5.4+)

To enable an installed module:

php5enmod <modulename>
To disable an installed module:

php5dismod <modulename>
Ubuntu 16.04 (php7) and higher:

To enable an installed module:

phpenmod <modulename>
To disable an installed module:

phpdismod <modulename>
Reload Apache

Remember to reload Apache2 after enabling/disabling:

service apache2 reload
