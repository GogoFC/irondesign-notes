# Logical Volume Manager

### Extend the size of a partition.

Display information about volume groups
```sh
sudo vgs
```
Display volume group information
```sh
sudo vgdisplay ubuntu-vg
```

![vgs](images/vgs.png)
