# Firefox ESR

### Install Firefox-ESR

```sh
pkg_add firefox-esr
```
### Firefox ESR description.

Firefox Extended Support Release (ESR) is an official version of Firefox developed for large organizations like universities and businesses that need to set up and maintain Firefox on a large scale. Firefox ESR does not come with the latest features but it has the latest security and stability fixes.

![esr](images/firefoxesr.png)

[Link to above description at support.mozilla.org](https://support.mozilla.org/en-US/kb/switch-to-firefox-extended-support-release-esr)

### FireFox Plugins

![adbp](images/adblockplus.png)

To block ads install [Adblock Plus](https://addons.mozilla.org/en-US/firefox/addon/adblock-plus/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
) or [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
).

To autodelete cookies when cookies aren't being used when switching a tab install [Cookie Autodelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)

# TOR

### Install TOR Browser

```sh
pkg_add tor-browser
```

For better privacy use [TOR](https://www.torproject.org/) Browser.

TOR Browser was developed to do the following:

- BLOCK TRACKERS
- DEFEND AGAINST SURVEILLANCE
- RESIST FINGERPRINTING
- MULTI-LAYERED ENCRYPTION
- BROWSE FREELY


TOR Project Mission statement:

> To advance human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding.

![tor](images/tor.png)

# Browser hardening

Browser [hardening](https://youtu.be/9t6jImyvnQk) boils down to editing Browser settings to disallow Websites to track your activity among other things. 


This [Article](https://unixsheikh.com/articles/choose-your-browser-carefully.html) talks about Browser privacy, it reviews many Browsers, and gives options to choose from. It mentions this [Firefox Privacy Guide](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies/) which uses [the arkenfox user.js](https://github.com/arkenfox/user.js).

    


