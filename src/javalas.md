# Java

### Install Oracle Java 1.8 on CentOS 7

Remove everything Java if needed.

```sh
sudoo yum -y remove java*
```

Download Oracle Java `.rpm`

[https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)

Install:

```sh
sudo rpm -ivh jdk-8u202-linux-x64.rpm
```
Check:
```sh
rpm -q --whatprovides java
```
```sh
java -version
```

If installing more versions of Java, choose default.

```sh
sudo update-alternatives --config java
```

### Add enviromentals 

Edit file: 

```sh
nano /home/user/.bash_profile
```
Add following at the end:

```sh
export JAVA_HOME=/usr/java/jdk1.8.0_202-amd64
export PATH=$JAVA_HOME/bin:$PATH
```

Edit:

```sh
nano /etc/environment
```
Add:

```sh
JAVA_HOME="/usr/java/jdk1.8.0_202-amd64"
```

Check:

```sh
echo $JAVA_HOME
```


   
