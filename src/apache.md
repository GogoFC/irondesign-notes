![apache](images/apache.png)

# Apache

### Modules

List available modules (Debian/Ubuntu)
```sh
ls /etc/apache2/mods-enabled/
```
Enable mod by sym link
```
ln -s /etc/apache2/mods-available/headers.load /etc/apache2/mods-enabled/
```

### HTTP 2

[Enable HTTP/2](https://www.howtoforge.com/how-to-enable-http-2-in-apache/) (Ubuntu 20)

```sh
sudo apt-get install php7.4-fpm && a2dismod php7.4 && a2enconf php7.4-fpm && a2enmod proxy_fcgi && a2dismod mpm_prefork && a2enmod mpm_event && a2enmod ssl && a2enmod http2 && systemctl restart apache2
```
Add `Protocols h2 http/1.1` line inside virtual host tags.

```sh
<VirtualHost *:443>
  ServerName irondesign.dev
  DocumentRoot /var/www/public_html/iron
  SSLEngine on
  SSLCertificateKeyFile /path/to/private.pem
  SSLCertificateFile /path/to/cert.pem
  SSLProtocol all -SSLv3 -TLSv1 -TLSv1.1
  Protocols h2 http/1.1
</VirtualHost
```

Check

```sh
curl -I -L --http2 https://irondesign.dev
```
