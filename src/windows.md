![windows](images/windows.png)

# Windows

### EFI bootloader repair

[Toms Hardware Windows Boot Repair](https://forums.tomshardware.com/faq/how-to-repair-efi-bootloader-in-windows-10.32751)

``` shell
BOOTREC /FIXMBR
BOOTREC /FIXBOOT
BOOTREC /SCANOS
BOOTREC /REBUILDBCD
```

To make it work I ran Automatic Repair again, which didn't work firts time around, FreeBSD dual boot broke my EFI boot m


`msinfo32` System Information App


### [Scoop](https://scoop.sh/)
```sh
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
```
```sh
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
```
```sh
scoop install youtube-dl
```


