# Upgrade OpenBSD 

[Upgrade Guide: 6.9 to 7.0](https://www.openbsd.org/faq/upgrade70.html)

OpenBSD posts a upgrade guide documentation link for the latest release on their [faq](https://www.openbsd.org/faq/index.html) page.

# Summary:

- Verify `/usr` partition space.

- Run `sysupgrade`

- Run `sysmerge` if needed.

- Run `pgk_add -u`



# Upgrade process:

### 1. Verify space

To verify at least 1.1 GB of space on `/usr` run the following command:

```sh
df -h /usr
```

### 2. Upgrade

Upgrade the system by running:

```sh
sysupgrade
```
### 3. Auto-reboot

System will reboot and run `sysmerge` and you can proceed to step 4. In some cases, configuration files cannot be modified automatically, then run `sysmerge` ater reboot.

### 4. Update packages

Run `pgk_add -u` to updade all packages, `reboot`, why not, enjoy.


# Screenshots

### Check space on `/usr` partition

![sysupgrade](images/usrspace.png)

### Sysupgrade running

![sysupgrade](images/sysupgrade.png)
