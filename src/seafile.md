# Seafile


### Admin password reset script

Enter a new valid email or existing.

```sh
/opt/seafile/seafile-server-8.0.0/reset-admin.sh
```


### Log files locations

Database info:

`/opt/seafile/conf/seafile.conf`

Logs:

`/opt/seaflie/logs`


### Docs

[Enabling HTTPS with Nginx](https://manual.seafile.com/deploy/https_with_nginx/)




### Crontab

Run on the 1st of every Month at 2:15 AM
 
`crontab -e`

```sh
15 2 1 * * certbot renew --post-hook 'systemctl restart nginx'
```

### UI upload error

Set URL to:

```sh
SERVICE_URL 		http://irondesign.dev/ 
FILE_SERVER_ROOT	https://irondesign.dev/seafhttp
```


`https://irondesign.dev/sys/web-settings/`

### Nginx seafile.conf

```sh
log_format seafileformat '$http_x_forwarded_for $remote_addr [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" $upstream_response_time';

server {
    listen       80;
    server_name  irondesign.dev www.irondesign.dev;
    rewrite ^ https://$http_host$request_uri? permanent;    # Forced redirect from HTTP to HTTPS

    server_tokens off;      # Prevents the Nginx version from being displayed in the HTTP response header
	

	proxy_set_header X-Forwarded-For $remote_addr; #Commented out. After uncommenting icons showed up.
}

server {
    listen 443 ssl http2;
    ssl_certificate /etc/letsencrypt/live/irondesign.dev/fullchain.pem;    # Path to your fullchain.pem
    ssl_certificate_key /etc/letsencrypt/live/irondesign.dev/privkey.pem;  # Path to your privkey.pem
    server_name irondesign.dev www.irondesign.dev;
    server_tokens off;

    # HSTS for protection against man-in-the-middle-attacks
    # https://manual.seafile.com/deploy/https_with_nginx/#advanced-tls-configuration-for-nginx-optional
    #
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";



    location / {
         proxy_pass         http://127.0.0.1:8000;
         proxy_set_header   Host $host;
         proxy_set_header   X-Real-IP $remote_addr;
         proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_set_header   X-Forwarded-Host $server_name;
         proxy_set_header   X-Forwarded-Proto $scheme;
         proxy_read_timeout  1200s;

         proxy_set_header   X-Forwarded-Proto https;

	 #added for optimization nginx -t complained
	 proxy_headers_hash_max_size 512;
	 proxy_headers_hash_bucket_size 128;

         # used for view/edit office file via Office Online Server
         client_max_body_size 0;

         access_log      /var/log/nginx/seahub.access.log seafileformat;
         error_log       /var/log/nginx/seahub.error.log;
    }

    location /seafhttp {
         rewrite ^/seafhttp(.*)$ $1 break;
         proxy_pass http://127.0.0.1:8082;
         client_max_body_size 0;
         proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_connect_timeout  36000s;
         proxy_read_timeout  36000s;

        access_log      /var/log/nginx/seafhttp.access.log seafileformat;
        error_log       /var/log/nginx/seafhttp.error.log;
    }
    location /media {
        root /opt/seafile/seafile-server-latest/seahub;
    }
    location /seafdav {
        proxy_pass         http://127.0.0.1:8080/seafdav;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_read_timeout  1200s;

        client_max_body_size 0;

        access_log      /var/log/nginx/seafdav.access.log seafileformat;
        error_log       /var/log/nginx/seafdav.error.log;
    }
}
```

[Proxy headers article](https://support.f5.com/csp/article/K51798430)

[Using the Forwarded header](https://www.nginx.com/resources/wiki/start/topics/examples/forwarded/)

