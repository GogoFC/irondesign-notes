
![termux](images/termux.png)

# [Termux](https://termux.com/)

## Add to PATH

Edit `.bash_profile` file. 

Append the PATH to `PATH=$PATH:$HOME/.local/bin:$HOME/bin,`. 

End result will be `PATH=$PATH:$HOME/.local/bin:$HOME/bin:/home/ec2-user/pear/bin`

`PATH="$PATH:/data/data/com.termux/files/home/.cargo/bin"`


- [difference-between-bashrc-and-bash-profile](https://superuser.com/questions/183870/difference-between-bashrc-and-bash-profile/183980#183980)


- [how-to-correctly-add-a-path-to-path](https://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path)

`PATH=$PATH:~/opt/bin`
or
`PATH=~/opt/bin:$PATH`

depending on whether you want to add ~/opt/bin at the end (to be searched after all other directories, in case there is a program by the same name in multiple directories) or at the beginning (to be searched before all other directories).

You can add multiple entries at the same time. `PATH=$PATH:~/opt/bin:~/opt/node/bin` or variations on the ordering work just fine. Don't put export at the beginning of the line as it has additional complications (see below under “Notes on shells other than bash”).

If your PATH gets built by many different components, you might end up with duplicate entries.

- [how-to-add-home-directory-path-to-be-discovered-by-unix-which-command](https://unix.stackexchange.com/questions/25605/how-to-add-home-directory-path-to-be-discovered-by-unix-which-command)

- [remove-duplicate-path-entries-with-awk-command](https://unix.stackexchange.com/questions/40749/remove-duplicate-path-entries-with-awk-command)


- [unix-shell-function-for-adding-directories-to-path](https://codereview.stackexchange.com/questions/88236/unix-shell-function-for-adding-directories-to-path)


- [how-to-add-directory-to-path-in-linux](https://linuxize.com/post/how-to-add-directory-to-path-in-linux/)


