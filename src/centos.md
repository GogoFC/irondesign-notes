![centos](images/centos.png)

# CentOS

### Update CentOS 8

Downloads and caches metadata for enabled repositories.

```sh
sudo dnf makecache
```

Checks for updates.
```sh
sudo dnf check-update
```

Updates CentOS 8.
```sh
sudo dnf update
```

[DNF Command Reference](https://dnf.readthedocs.io/en/latest/command_ref.html)

