# Linux
### User Account Info and Login Details

```sh
$ id root

uid=0(root) gid=0(root) groups=0(root)
```

```sh
$ groups root
root : root
```

```sh
$ getent passwd root
root:x:0:0:root:/root:/bin/bash
```

```sh
lslogins -u
 UID USER   PROC PWD-LOCK PWD-DENY LAST-LOGIN GECOS
   0 root     74                              root
```

```sh
$ who -u
root   pts/0        2021-12-24 17:43   .          1560 (8.8.8.8)
```
```sh
w
 18:00:19 up 23 min,  1 user,  load average: 0.00, 0.02, 0.08
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
user   pts/0    88.808.808.888     17:43    1.00s  0.09s  0.01s w
```
```sh
$ last
```
```sh
du -sh
```
