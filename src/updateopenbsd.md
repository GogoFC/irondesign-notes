# Update packages

To update all packages run:

```sh
pkg_add -u
```
### Updating packages

![updt](images/pkgupdate.png)

### Docs:

[Package Managment](https://www.openbsd.org/faq/faq15.html)
