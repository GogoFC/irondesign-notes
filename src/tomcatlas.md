# Tomcat

### Install Tomcat 8.5 on CentOS 7

[HowtoForge Manual](https://www.howtoforge.com/tutorial/how-to-install-tomcat-8.5-on-centos-7.3/#comments)

Change directory.
```sh
cd /opt
```
Install `wget`.
```sh
sudo yum install wget
```
Get Tomcat.
```sh
sudo wget https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.76/bin/apache-tomcat-8.5.76.tar.gz
```
Untar.
```sh
sudo tar -xzvf apache-tomcat-8.5.76.tar.gz
```
Rename.
```sh
sudo mv apache-tomcat-8.5.76/ tomcat/
```

### Create user and group.

Add `tomcat` group and `tomcat` user.
```sh
groupadd tomcat
useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
```
Change mod.
```sh
chmod 775 -R tomcat/
```
Change ownership.
```sh
sudo chown -hR tomcat:tomcat /opt/tomcat
```
### Firewall. 

Add rule.
```sh
sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp
```
Reload.
```sh
sudo firewall-cmd --reload
```
Check port.
```sh
sudo firewall-cmd --list-ports
```

### At this point you can test Tomcat.

Start.
```sh
sudo bash /opt/tomcat/bin/startup.sh
```
Check if it's running.
```sh
netstat -plntu
```
![run](images/tomcatrunning.png)


URL:

[http://localhost:8080](http://localhost:8080)



Stop.
```sh
sudo bash /opt/tomcat/bin/shutdown.sh
```

### Run as service

Create a new file 'tomcat.service'

```sh
sudo nano /etc/systemd/system/tomcat.service
```

Paste the configuration.

```sh
[Unit]
Description=Apache Tomcat 8 Servlet Container
After=syslog.target network.target
 
[Service]
User=tomcat
Group=tomcat
Type=forking
Environment=CATALINA_PID=/opt/tomcat/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh
Restart=on-failure
 
[Install]
WantedBy=multi-user.target
```

Start and enable at boot.

```sh
sudo systemctl daemon-reload
sudo systemctl start tomcat
sudo systemctl enable tomcat
```

Check.
```sh
sudo systemctl status tomcat
```

  
