![iron](images/iron.svg)

## Introduction

Notes on using different Operating Systems and services relating to DevOps, for reference. 


[**OpenBSD**](https://www.openbsd.org/) - a FREE, multi-platform 4.4BSD-based UNIX-like operating system...

[**FreeBSD**](https://www.freebsd.org/) - an operating system used to power modern servers...

[**Debian**](https://www.debian.org/) - The Universal operating system...

[**Nginx**](https://nginx.org/en/) - [engine x] is an HTTP and reverse proxy server...

[**ZFS**](https://openzfs.org/wiki/Main_Page) - OpenZFS is an advanced file system and volume manager...

[**Rust**](https://www.rust-lang.org/) - A language empowering everyone to build reliable and efficient software.

[**Termux**](https://termux.com/) - an Android terminal emulator and Linux environment app...
