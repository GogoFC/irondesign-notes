# Commands Cheat Sheet

### Key Sequence

| Key Sequence |  Description  |
|  --  |  --  |
| `ctrl`+`alt`+`F1`     |    Exit X and switch to tty0    |
| `ctrl`+`alt`+`F5`     |    Switch back to X    |

### Commands

| Command |  Description  |
|  --  |  --  |
| `rcctl ls on`  | List running services |
| `rcctl disable xenom` | Disable xenom service |
| `rcctl enable xenom`  | Enable xenom service |
| `rcctl restart` | Restarts a service |
| `rcctl reload`  | Reloads config file. Doesn't restart |
| `pkg_add -u` | Update packages |
| `syspatch` | Run security updates |
| `sysupgrade` | Upgrade to new release |
| `shutdown -p now` | Power off system |
