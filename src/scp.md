# [SCP - Secure Copy Protocol](https://www.openssh.com/)

---

# Using SCP to copy data.


Copy a directory and it's contents recursively. 


```bash
sudo scp -r /home/user/data user@10.0.0.1:/home/user
```

Copy a file from remote directory to current directory.
```sh
scp user@10.0.0.1:/home/user/file.md .
```

- SCP does not support resuming if the transfer is interrupted. 

- If a file only got transferred partially it can be resumed with [Rsync](./rsync.md) using the `-P` flag or [SFTP](./sftp.md) using the `-a` flag.

## Flags

`-p`  Preserve modification time timestamps.

# Docs

[Manual Page - SCP](https://man.openbsd.org/scp.1)
