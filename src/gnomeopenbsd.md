# Install GNOME on OpenBSD

You may skip this step and use the X Window Manager that is installed by default on OpenBSD and proceed to installing [Firefox ESR](./firefox.md) and [ProtonVPN](./protonvpn.md). 

If you prefer more to have more Desktop Environment features you can install GNOME by following the instructions below.

### 1. Install

Install GNOME with extras and nano text editor.

```sh
pkg_add gnome gnome-extras nano
```

### 2. Config

Edit `/etc/login.conf` file and add the following below:

```sh
nano /etc/login.conf
```

```sh
gnome:\
	:datasize-cur=1024M:\
	:tc=default:

gdm:\
	:tc=xenodm:
```
This will create a "gnome" login class and set proper limits needed to run GNOME, and also give gdm the default limits of xenodm.

### 3. Setup Users

Then add a user to the 'gnome' login class. 

Do this for any user that will be running GNOME.

```sh
usermod -L gnome ${username}
```

### 4. Enable GNOME

Then run the following to disable 'xenodm' and enable 'gdm' (GNOME display manager) along with packages necessary to run GNOME.

```sh
rcctl disable xenodm
```
```sh
rcctl enable multicast messagebus avahi_daemon gdm
```
```sh
reboot
```


[Gnome Port Notes](https://openports.pl/path/meta/gnome)

