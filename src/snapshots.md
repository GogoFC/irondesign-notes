![logo](images/openzfs.png)

# [ZFS](https://openzfs.org/wiki/Main_Page) Snapshots



On Linux regular users have permissions to: 

- Take Snapshots of their Home directory Dataset
- Destroy the Snapshots
- Rollback Snapshots
- Receive unmounted Snapshots for storing them as back up from elsewhere.
- Send Snapshots to remote locations.

 

# Taking Snapshots

To take a snapshot of dataset `rpool/USERDATA/user3853_2bv9ey` enter a name after `@`.

```sh
zfs snapshot rpool/USERDATA/user3853_2bv9ey@SnapshotName
```

# Destroying Snapshots


Destroy a snapshot.

```sh
zfs destroy rpool/USERDATA/user3853_2bv9ey@t353
```

Delete all snapshots on dataset `user3853_2bv9ey`, by adding `%` after `@`.
```sh
zfs destroy rpool/USERDATA/user3853_2bv9ey@%
```
# Rolling back Snapshots

Rolling back to a Snapshot will revert the Dataset back to what it was at that time. Data currently on it will be replaced by data from the Snapshot, so if you need the current data move it elsewhere. All consequent Snapshots will be destroyed, ZFS will complain you have to use `-r` flag.

```sh
zfs rollback -r rpool/USERDATA/user3853_2bv9ey@t
```



# Nomount flag

When sending and receiving Snapshot use `-u` flag to tell ZFS not to try to mount it. This also prevents the creation of empty directories where the dataset would have been mounted. 

```sh
zfs send rpool/USERDATA/user3853_2bv9ey@today | zfs recv -u rpool/USERDATA/user3853_2bv9ey/newdataset
```

# Send over SSH
```sh
zfs send rpool/USERDATA/user0_sosjq3@snap | ssh user3853@localhost zfs recv -u rpool/USERDATA/user3853_2bv9ey/snapnomount
```
# Incremental Snapshot

When sending incremental Snapshots use the flag `-i`, and use the flag `-F` for receiving. 

Flag `-F` forces rollback or Dataset, as the data has now changed. 

```sh
zfs send -i rpool/USERDATA/user0_sx0jq9@inc rpool/USERDATA/user0_sx0jq9@inc4 | ssh user3853@localhost zfs recv -F rpool/USERDATA/user3853_2bv9ey/snapnomount
```

# List Snapshots

List all Snapshots including received unmounted ones.

```sh
zfs list -t snapshot
```

# List Datasets

Datasets will show mountpoints but will not be mounted.

```sh
zfs list
```

# Show mounted

Shows actual mounted Datasets and mount points. 

```sh
zfs mount
```

# List a Dataset

Using the `-r` flag to recursively list all Datasets on `user3853_2bv9ey`.

```sh
zfs list -r rpool/USERDATA/user3853_2bv9ey
```

# Send to remote

Using `sudo zfs recv` to mount the cloned Dataset with proper permissions.

```sh
zfs send rpool/USERDATA/user3853_2bv9ey/snapreceive@nomount | ssh user@irondesign.dev sudo zfs recv zroot/usr/home/extra/received
```
Send Snapshot, don't mount, and change the name of Snapshot at receiving Machine to `@new`.

```sh
zfs send rpool/USERDATA/user3853_2bv9ey/snapreceive@nomount | ssh user23@irondesign.dev sudo zfs recv -u zroot/usr/home/extra/unmouted@new
```

# Documentation

[illumos ZFS Administration Guide ](https://illumos.org/books/zfs-admin/snapshots.html)

## Misc. 

- Users have permissions to receive Snapshots, but Snapshots will not be mounted. 

- Users don't have permission to mount. 

- Users do not have permission to promote Snapshots to Clones as that would require permissions to mount Datasets.

- There is no limit to how many Snapshots you can take but they do take up space, so they are limited by Quota. 
