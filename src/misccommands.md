# Misc Commands

```sh
# /etc/rc.d/netif restart
```

# /etc/group

Add users to a group syntax
```ksh
pw group mod {groupNameHere} -m {userNameHere1,userNameHere2,...}
```


### Shows which groups user belongs to
`groups user23`

### Add a group
`pw groupadd datoteka`

### Show users who belong to group datoteka
pw groupshow datoteka

### Add user user23 to group datoteka
pw groupmod datoteka -M user23

### Using id(1) to Determine Group Membership
`id user23`
returns:
`uid=1001(user23) gid=1001(user23) groups=1001(user23),0(wheel),1002(datoteka)`

### Delete a group
`pw groupdel datoteka`

```ksh
# portsnap fetch
# portsnap extract
# portsnap fetch update
```

