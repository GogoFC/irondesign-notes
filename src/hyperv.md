![hyper-v](images/windows.png)

# Hyper-V

[Microsoft Docs](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/user-guide/setup-nat-network)

# Create Virtual Switch and NAT in PowerShell

Create an internal virtual switch named "NAT_Switch" that can talk to external network, a private switch can not.
```sh
New-VMSwitch -SwitchName "NAT_Switch" -SwitchType Internal
```
Get switch ifIndex, in this case 29.
```sh
Get-NetAdapter
```

Add 10.0.0.1 as the gateway IP of the switch.
```sh
New-NetIPAddress -IPAddress 10.0.0.1 -PrefixLength 24 -InterfaceIndex 29
```
Enable NAT
```sh
New-NetNat -Name NAT_for_VMs -InternalIPInterfaceAddressPrefix 10.0.0.0/24
```
Removing NAT

```sh
Get-NetNat
```

```sh
Remove-NetNAT -Name NAT_Switch
```
# Links:

[https://rdr-it.com/en/hyper-v-create-a-nat-switch/](https://rdr-it.com/en/hyper-v-create-a-nat-switch/)

[https://4sysops.com/archives/native-nat-in-windows-10-hyper-v-using-a-nat-virtual-switch/](https://4sysops.com/archives/native-nat-in-windows-10-hyper-v-using-a-nat-virtual-switch/)
