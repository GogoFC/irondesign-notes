# Syncthing

[manpage](https://manpages.debian.org/testing/syncthing/syncthing-faq.7.en.html)

[docs](https://docs.syncthing.net/users/syncthing.html)

HOW DO I ACCESS THE WEB GUI FROM ANOTHER COMPUTER?

The default listening address is 127.0.0.1:8384, so you can only access the GUI from the same machine. This is for security reasons. Change the GUI listen address through the web UI from 127.0.0.1:8384 to 0.0.0.0:8384 or change the config.xml:
```xml
<gui enabled="true" tls="false">


  <address>127.0.0.1:8384</address>
```

to
```xml
<gui enabled="true" tls="false">


  <address>0.0.0.0:8384</address>
```

Then the GUI is accessible from everywhere. You should set a password and enable HTTPS with this configuration. You can do this from inside the GUI.

If both your computers are Unix-like (Linux, Mac, etc.) you can also leave the GUI settings at default and use an ssh port forward to access it. For example,

`$ ssh -L 9090:127.0.0.1:8384 user@othercomputer.example.com`


will log you into othercomputer.example.com, and present the remote Syncthing GUI on `http://localhost:9090` on your local computer.

If you only want to access the remote gui and don’t want the terminal session, use this example,

`$ ssh -N -L 9090:127.0.0.1:8384 user@othercomputer.example.com`


If only your remote computer is Unix-like, you can still access it with ssh from Windows.

Under Windows 10 (64 bit) you can use the same ssh command if you install the Windows Subsystem for Linux <https://docs.microsoft.com/windows/wsl/install-win10>.

Another Windows way to run ssh is to install gow (Gnu On Windows) <https://github.com/bmatzelle/gow>. 

The easiest way to install gow is with the chocolatey <https://chocolatey.org/> package manager.

