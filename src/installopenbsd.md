# Installing OpenBSD


Installing OpenBSD is very simple, involves choosing appropriate options by pressing appropriate keys.

Starting the installer is done by typing the letter `i` and pressing `enter`.

Following option is choosing the keyboard layout. 

Hit `enter` to choose the `[default]` option and proceed, otherwise select `L` to list available Keyboard layouts. 

The `[default]` options is US QWERTY keyboard. 

Following the install steps below is sufficient, screenshots are provided below. 

[Video](https://youtu.be/fGA2wGjOfTM)

### Installation Steps: 

Select an option and hit `enter` to proceed:

| No   |  Options  |  Choose  |  Description |
| ---- | --------- | -------- | ------------ |
|1.| Installer: | `i` + `enter` | Starts the installer |
|2.| Keyboard: | `[default]` + `enter` | Selects QWERTY keyboard |
|3.| Hostname: | `OpenBSD-VM` + `enter`| Gives computer a name |
|4.| Network: | `[em0]` + `enter` | Selects `em0` as default Network device |
|5.| IPv4: | `[dhcp]` + `enter` | Selects DHCP for IPv4 |
|6.| IPv6: | `[none]` + `enter` | Selects not to use IP version 6 |
|7.| Network: | `[done]` + `enter` | Done with setting up network |
|8.| Domain name: | `catsrunning.com` + `enter` | Domain name of server |
|9.| Password: | `5235678` + `enter` | Sets root password as `5235678` |
|10.| Password: | `5235678` + `enter` | Again |
|11.| sshd: | `[yes]` + `enter` | Starts SSH daemon at boot |
|12.| X: | `[yes]` + `enter` | Installs Xorg on sytem |
|13.| xenodm: | `[yes]` + `enter` | Starts X automatically. If no is selected X can be started by typing `startx` |
|14.| User: | `john` + `enter` | Creates user 'john' |
|15.| Name: | `John Smith` + `enter` | Saves Full name of user |
|16.| Password: | `123456` + `enter` | Creates password for user john |
|17.| Password: | `123456` + `enter` | Again |
|18.| Root SSH: | `[no]` + `enter` | Select `no` to disallow root logins and use user john to SSH into VM |
|19.| Disk: | `[wd0]` + `enter` | Selects the disk `wd0` to install OpenBSD |
|20.| Whole: | `[whole]` + `enter` | Uses whole disk with MBR. To use GPT select `G` if installing on a real and new Computer |
|21.| Auto: | `[a]` + `enter` | Selects auto layout of partitions on disk |
|22.| Location of Sets: | `[http]` + `enter` | Selects `http` as location of sets to be installed |
|23.| Proxy: | `[none]` + `enter` | No proxy server selected |
|24.| HTTP Server: | `https://mirror.aarnet.edu.au` + `enter` | Selects an Australian mirror |
|25.| Server directory: | `[pub/OpenBSD/7.0/amd64]` + `enter` | Hit enter, it will automatically select the needed directory `[pub/OpenBSD/7.0/amd64]` as location of sets |
|26.| Select sets: | `[done]` + `enter` | Hit enter to select to install all sets. If installing form `cd` instead `http` you would type `yes` here and proceed. |
|27.| Location of Sets: | `[done]` +`enter` | Done, no additional sets to install |
|28.| Timezone: | `[Australia/Brisbane]` + `enter` | Type appropriate Time Zone and hit `enter` |
|29.| Reboot: | `[reboot]` + `enter` | Reboot the system. Remove the DVD boot disk if it boots to install again. |

After OpenBSD is installed go to [Post Install](./postinstall.md)

### Copy paste options:

Mirror:

```sh
https://mirror.aarnet.edu.au
```
Time Zone: 

```sh
Australia/Brisbane
```

# Screenshots

### 1. Start the Installer

![VBOX](images/VirtualBox1.png)

### 2. Choose a Keyboard layout

![VBOX](images/VirtualBoX2.png)

### 3. Enter a Hostname

![VBOX](images/VirtualBox3.png)

### 4. Choose Network Interface

![VBOX](images/VirtualBox4.png)

### 5. Selecd DHCP for IPv4

![VBOX](images/VirtualBox5.png)

### 6. IPv6 not used

![VBOX](images/VirtualBox6.png)

### 7. Done with Network

![VBOX](images/VirtualBox7.png)

### 8. Enter a Domain Name

![VBOX](images/VirtualBox8.png)

### 9. Enter assword for root account

![VBOX](images/VirtualBox9.png)

### 10 Enter assword again

![VBOX](images/VirtualBox10.png)

### 11. Start SSH daemon at boot

![VBOX](images/VirtualBox11.png)

### 12. Install X Window System

![VBOX](images/VirtualBox12.png)

### 13. Start X by xenodm automatically

![VBOX](images/VirtualBox13.png)

### 14. Set up a user

![VBOX](images/VirtualBox14.png)

### 15. Enter full name of user

![VBOX](images/VirtualBox16.png)

### 16. Enter password for user

![VBOX](images/VirtualBox17.png)

### 18. Allow root SSH login

![VBOX](images/VirtualBoX18.png)

### 19. Select root disk to install OpenBSD

![VBOX](images/VirtualBox19.png)

### 20. Selects whole disk with MBR

![VBOX](images/VirtualBox20.png)

### 21. Use automatic partition layout

![VBOX](images/VirtualBox21.png)

### 22. Selects http as location of sets to be installed

![vbox](images/VirtualBox_httpsets.png)

### 23. No proxy server

![vb](images/VirtualBox_proxy.png)

### 24. 25. 

24. Set `https://mirror.aarnet.edu.au` as http server mirror

25. Hit `enter` to select [pub/OpenBSD/7.0/amd64] as location of sets

![vbox](images/VirtualBox_21a.png)


### 26. Select sets

![VBOX](images/VirtualBox24.png)

### 26. In case sets are being installed from a cd type `yes`

![VBOX](images/VirtualBox25.png)

### 27. Done installing sets

![VBOX](images/VirtualBox28.png)

### 28. Set Time Zone

![VBOX](images/VirtualBoxtimezone.png)


### Relinking to create a unique kernel

![VBOX](images/VirtualBox30.png)

### `reboot`

![VBOX](images/VirtualBox31.png)

### After reboot

![VBOX](images/VirtualBox32.png)

# Default X Window Manager installed

![VBOX](images/VirtualBox33.png)


![VBOX](images/VirtualBox34.png)


![VBOX](images/VirtualBox35.png)


![VBOX](images/VirtualBox36.png)






# Video


