![rust](images/rust.png)

# Rust

### mdbook install error

warning: spurious network error (1 tries remaining):


Adding

```sh
[http]
multiplexing = false
```

to `~/.cargo/config` fixed it.

[reddit rust comments cratesio timing out](https://www.reddit.com/r/rust/comments/ff5fs5/cratesio_timing_out/)


### [cargo-update](https://crates.io/crates/cargo-update)

Install
```sh
cargo install cargo-update
```

Update all 

```sh
cargo install-update -a
```

Specific packages

```sh
cargo install-update crate1 crate2
```

