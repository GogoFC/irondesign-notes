# Notes

https://lists.freebsd.org/pipermail/freebsd-questions/2014-August/260678.html

Both are correct. According to rc.conf(5):

kld_list    (str) A list of kernel modules to load right after the local
            disks are mounted.  Loading modules at this point in the boot
            process is much faster than doing it via /boot/loader.conf
            for those modules not necessary for mounting local disk.

