![nginx](images/nginx.png)


# Server Blocks

[Nginx on Debian - Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-10)

```sh
sudo nano /etc/nginx/sites-available/your_domain
```

```sh
sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/
```

```sh
sudo nano /etc/nginx/nginx.conf
```

```sh
...
http {
    ...
    server_names_hash_bucket_size 64;
    ...
}
...
```


# Apache Benchmark

```sh
sudo apt install apache2-utils
```
Start Benchmark



```sh
ab -n 1000 -c 100 https://irondesign.dev/
```
-c count 100 requests per second

-n number 1000 requests total

See open file limits

```sh
ulimit -n
```

```sh
ulimit -a
```
Edit

```sh
events {
worker_connections 1024;
}
```
# Restarting nginx

Gracefully reload nginx web server:

```sh
sudo systemctl reload nginx
```

```sh
service nginx reload
```

```sh
/etc/init.d/nginx reload
```

```sh
nginx -s reload
```

Fully restart nginx web server:

```sh
sudo systemctl restart nginx
```
```sh
service nginx restart
```

```sh
/etc/init.d/nginx restart
```

Status

```sh
sudo systemctl status nginx
```

```sh
service nginx status
```



