# WiFi

## Set up a trunk  device

Trunk interface automatically connects to one of multiple Ethernet or wifi devices.

> Trunks are virtual interfaces consisting of one or more network interfaces. In this section, our example will be a laptop with a wired [bge0](https://man.openbsd.org/bge) interface and a wireless [iwn0](https://man.openbsd.org/iwn) interface. We will build a [trunk(4)](https://man.openbsd.org/trunk) interface using both of them. The wired and wireless interfaces must be connected to the same layer two network.

[Wireless Networking Docs](https://www.openbsd.org/faq/faq6.html#Wireless)

### 1. Devices

List devices

```sh
ifcongig
```

The listed devices are

- Ethernet `hostname.em0`

- WiFi `hostname.iwm0`

- Trunk `hostname.trunk0`

![ss](images/hostnames.png)


### 2. Ethernet device

Edit the `/etc/hostname.em0` to contain only `up`

![ss](images/hostname.em0.png)

### 3. Wifi device

Edit the `/etc/hostname.iwm0` add WiFi networks. One per each line separated by a space `join $SSID wpakey $PASSWORD` and add `up` on the last line. In the photo below passwords are deleted but they come after wpakey.

![ss](images/hostname.iwm0.png)

### 4. Trunk device

Create a new file for the trunk device.

```sh
nano /etc/hostname.trunk0
```
Add the following lines to it. Change the names of interfaces as needed to match your own.

```sh
trunkproto failover trunkport em0
trunkport iwm0
inet autoconf
```

![ss](images/hostname.trunk0.png)

### 5. Restart network


Run

```sh
/etc/netstart
```
Should connect to WiFi, if not `reboot`.

