![opebsd](images/openbsd10.png)

# Install [OpenBSD](https://www.openbsd.org/) on VirtualBox

Assumptions:

- Using `root` for installing packages.
- Disabling SSH root logins.
- Creating a regular user.

Some Specifics:

- Using `https://mirror.aarnet.edu.au` mirror during install
- Setting Time Zone as `Australia/Brisbane`

### Contents:

- [Getting OpenBSD](./getingopenbsd.md)
- [Virtual Box](./virtualboxopenbsd.md)
- [Install OpenBSD](./installopenbsd.md)
- [Post Install](./postinstallopenbsd.md)
- [Update Packages](./updateopenbsd.md)
- [Upgrade OpenBSD](./upgradeopenbsd.md)
- [GNOME](./gnomeopenbsd.md)
- [Firefox ESR](./firefoxopenbsd.md)
- [ProtonVPN](./protonvpnopenbsd.md)
- [Cheat Sheet](./cheatsheetopenbsd.md)
- [Video Links](./videolinksopenbsd.md)
