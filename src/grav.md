![grav](images/grav-logo2.png)

### Install dependencies

```sh
sudo apt install php-ctype php-dom php-gd php-json php-mbstring php-xml php-zip php-curl
```
a
```sh
php --ini
```
### Create a new user

```sh
sudo -u www-data bin/plugin login new-user
```

### Install via Composer
```sh
sudo apt install composer
```

```sh
which composer
```

```sh
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```

```sh
sudo php composer-setup.php --install-dir=/usr/bin --filename=composer
```

### Cache was annoying, after editng a website cache would persist until Ctrl+F5
```sh
sudo apt remove php-opcache
```

```sh
sudo apt remove php-apcu
```
