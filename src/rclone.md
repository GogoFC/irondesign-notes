![logo](images/rclone.png)

---

# Using [Rclone](https://rclone.org/) to sync data.

#### In this example we use:

- Rclone over SFTP.
- User SSH password. 

# Install

Install Rclone on Debian and Ubuntu.

```sh
sudo apt install rclone
```
Install Rclone on Windows and other Systems.

Go to [https://rclone.org/downloads/](https://rclone.org/downloads/)

# Configure Rclone

Create a New Remote, run rclone config. 

```sh
rclone config
```
![rclone config](images/rcloneconfig.png)

Choose option 26 for SSH.

![rclone 26](images/rclone26.png)

Enter Hostname or IP. 

![rclone hostname](images/rclonehostname.png)

Enter your username.

Hit `Enter` for default port 22.

Type `y` to enter your password. 

![rclone user](images/rcloneuser.png)

Enter your password.

Press `Enter` for the default value.

Type `n` to leave optional password blank.

![rclone pwd](images/rclonepassword.png)

Press `Enter` for the default ("false").

![rclone n](images/rclonen.png)

Press `Enter` for the default ("false") twice. 

![rclone cipher](images/rclonecipher.png)

Press `n` for advanced config. 

![rclone adv](images/rcloneadvanced.png)

Press `y` to confirm settings.

![rclone confirm](images/rcloneconfirm.png)

New "remote" is now created. 

Type `q` to quit.

![rclone done](images/rclonedone.png)

# Listing files and directories on remote

List directories in the home directory

```sh
rclone lsd extralayer:
```

Use `-R` flag to list directories and sub-directories recursively.

```sh
rclone lsd -R extralayer:
```

![lsd](images/rclonelsd.png)

List files in the home directory

```sh
rclone ls extralayer:
```

List directories and files in the home directory

```sh
rclone lsf extralayer:
```

List directories and files recursively.

```sh
rclone lsf -R extralayer:disks
```

# Use Rclone to sync data 

Clone a local directory named disks to a remote directory named disks.

```sh
rclone sync disks/ extralayer:disks/ -P
```
Flag `-P` shows progress.

![sync](images/rclonesync.png)

The command will sync everything from local to remote. If data is added or deleted locally Rclone will sync the changes to remote when you run `rclone sync` again. 

If the directory of the same name already exists on remote then Rclone will remove the files from remote directory that do not exist on local and upload the existing local files to remote, so make sure you don't overwrite something you don't want to overwrite accidentally.

**Important:** Use `--dry-run` flag beforehand to prevet data loss. 

Message "Not deleting as dry run is set" will display.

![dry](images/rclonedryrun.png)


# Rclone Browser

[https://github.com/kapitainsky/RcloneBrowser](https://github.com/kapitainsky/RcloneBrowser)

# Rclone Documentation

[Rclone sftp](https://rclone.org/sftp/)

[Rclone sync](https://rclone.org/commands/rclone_sync/)

[Rclone commands](https://rclone.org/commands/)
