![logo](images/borg_logo.svg)

---

# Using [Borg](https://www.borgbackup.org/) to backup data

#### In this example we use:

- Borg over SSH to set up remote repo.
- SSH keys to login. 

# Install

Install Borg on Debian and Ubuntu based Linux.
```sh
sudo apt install borgbackup
```
Install Borg on other Systems.

Go to [https://www.borgbackup.org/releases/](https://www.borgbackup.org/releases/)

# Set up remote repo.


### 1. Initialize a repository on remote host.
```sh
borg init -e repokey user118@65.21.93.164:/home/user118/repo
```
![init](images/borginit.png)



### 2. Backup the `~/disks` and `~/folder` directories into an archive called `first`.
```sh
borg create -v --stats user118@65.21.93.164:/home/user118/repo::first ~/disks ~/folder
```
![create 1](images/borgcreate1.png)

![create 2](images/borgcreate2.png)


### 3. List all archives in the repository.
```sh
borg list user118@65.21.93.164:/home/user118/repo
```
![list](images/borglistrepo.png)


### 4. List contents of archive named `third`.
```sh
borg list user118@65.21.93.164:/home/user118/repo::third
```
![list](images/borglistrepo2.png)


### 5. Restore the `third` archive to current directory.
```sh
borg extract user118@65.21.93.164:/home/user118/repo::third
```
![restore](images/borgextract.png)

### 6. Delete the `third` archive.
```sh
borg delete user118@65.21.93.164:/home/user118/repo::third
```
![delete](images/borgdelete.png)

Note that this does not free repo disk space.

### 7. Recover disk space by compacting the segment files in the repo.
```sh
ssh user118@65.21.93.164 borg compact /home/user118/repo
```

### 8. Delete the whole repository and the related local cache:
```sh
borg delete user118@65.21.93.164:/home/user118/repo
```

### 9. [SSHFS](./sshfs.md)

If it is not possible to install Borg on the remote host, it is still possible to use the remote host to store a repository by mounting the remote filesystem, for example, using sshfs:

This will mount remote directory `disks` to local directory `extra`.


    sshfs user118@65.21.93.164:/home/user118/disks ~/extra
    borg init -e repokey ~/extra/repo
    fusermount -u /home/{user}/extra

# Documentation

[Borg Quickstart](https://borgbackup.readthedocs.io/en/stable/quickstart.html)
