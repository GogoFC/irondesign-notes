# NextCloud

### Accessing NextCloud files using WebDAV

[Documentation](https://docs.nextcloud.com/server/22/user_manual/en/files/access_webdav.html#accessing-nextcloud-files-using-webdav)



### NextCloud snap data location

`/var/snap/nextcloud/common/nextcloud/data/cats`


### Mapping a Windows network drive

Using Windows Explorer 

```sh
https://domain.dev/owncloud/remote.php/webdav
```
```sh
https://domain.dev/owncloud/remote.php/dav/files/username/
```


Using Command Prompt

```sh
net use G: https://domain.dev/owncloud/remote.php/webdav /user:username password
```
```sh
net use Z: https://domain.dev/owncloud/remote.php/dav/files/username/ /user:username password
```

```sh
net use S: \\domain.dev@ssl\owncloud\remote.php\webdav /user:username password
```
```sh
net use S: \\domain.dev@ssl\owncloud\remote.php\dav\files\username /user:username password
```

![drive](images/mapnetworkdrive.png)


![drive](images/mapnetworkdriveurl.png)
