# Downloading OpenBSD

### 1. Getting OpenBSD

[Download OpenBSD](https://www.openbsd.org/faq/faq4.html#Download)

Different types of images are available. 

- In our case download the `cd70.iso` file because it contains signatures so the installer doesn't complain. 
- Use `http` instead of `cd0` source as option when installing to utilize signatures. 
- If you wish to install from cd type `yes` and hit enter instead. 

Explanation cited from the OpenBSD website below:
> The install70.iso and install70.img images do not contain an SHA256.sig file, so the installer will complain that it can't check the signature of the included sets:
> ```Directory does not contain SHA256.sig. Continue without verification? [no]```
> This is because it would make no sense for the installer to verify them. If someone were to make a rogue installation image, they could certainly change the installer to say the files were legitimate. If the image's signature has been verified beforehand, it is safe to answer "yes" at that prompt.


### 2. Save `cd70.iso` file.

Save the ISO file to use with Virtual Box and proceed to [Install OpenBSD](./install.md) or [Virtual Box](./virtualbox.md) section.

# Booting from USB drive.


To create a bootable USB drive for installing on a 'bare metal' machine download the `miniroot70.img` file and use `dd` command to burn the  file to a USB stick. If using a DVD download an `.iso` file instead.

Replace rsd6c with appropriate device. 

```sh
`dd if=miniroot70.img of=/dev/rsd6c bs=1M` 
```
---

On a Windows machine [Rufus](https://rufus.ie/en/) can be used to burn a ISO image to USB stick.

![rufus](images/rufus.png)

### Links

[Installation guide (FAQ4)](https://www.openbsd.org/faq/faq4.html)

[FAQ Index](https://www.openbsd.org/faq/index.html)


