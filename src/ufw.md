# [Uncomplicated Firewall](https://help.ubuntu.com/community/UFW)

```sh
sudo ufw app list
```
```sh
sudo ufw allow OpenSSH
```
```sh
sudo ufw enable
```
```sh
sudo ufw status
```
```sh
sudo ufw allow "Nginx HTTPS"
```

