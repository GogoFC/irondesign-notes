![dwm](images/dwm.png)


# Compile DWM on FreeBSD 13.0

Summary of instructions from this complete [**Guide**](https://horodistea.wordpress.com/2020/02/06/compiling-suckless-tools-on-freebsd/)

Change X11INC and X11LIB paths in each & every config.mk file to FreeBSD-adjusted form adding 'local':
```sh
X11INC=/usr/local/include
X11LIB=/usr/local/lib
```
Git clone software
```sh
$ mkdir ~/git
$ cd git
$ git clone git://git.suckless.org/dmenu
$ git clone git://git.suckless.org/slstatus
$ git clone git://git.suckless.org/st
$ git clone git://git.suckless.org/dwm
$ git clone git://git.suckless.org/sent
$ git clone git://git.suckless.org/surf
```

# Supplementing notes:

**Laptop:**
>HP EliteBook 840 G2
>Intel graphics

Add user "iron" to 'video' group

`sudo pw group mod video -m iron`

Enable Linux compatibility kernel modules at boot time,
add this line to `/etc/rc.conf` file.

```sh
linux_enable="YES"
```

Or to load modules right away run:
```sh
kldload linux64
kldload linux
```
Install packages
```sh
pkg install git xorg-server xorg-fonts-truetype gcr devel/glib20 xorg-fonts-type1 p5-X11-Xlib p5-PkgConfig xauth xrandr xinit libXft xrdb webkit2-gtk3 feh xf86-input-mouse xf86-input-keyboard linux-c7-libpng pkgconf ncurses xf86-input-libinput terminus-font drm-kmod xterm
```
Edit `/etc/rc.conf` file to contain:
```sh
kld_list=”i915kms”
```

# Create .xinitrc and .Xresources files in $HOME dir

In home directory run:

`ee .xinitrc`

and paste the following:

```sh
xrdb -load $HOME/.Xresources
feh --bg-fill ~/.wallpaper&
slstatus&
exec dwm
```
Run:
`ee .Xresources`

```sh
! Use a nice truetype font and size by default...
xterm*faceName: xos4 Terminus
xterm*faceSize: 12

! Every shell is a login shell by default (for inclusion of all necessary environment v
xterm*loginshell: true

! I like a LOT of scrollback...
xterm*savelines: 16384

! double-click to select whole URLs 😀
xterm*charClass: 33:48,36-47:48,58-59:48,61:48,63-64:48,95:48,126:48

! DOS-box colours...
xterm*foreground: rgb:a8/a8/a8
xterm*background: rgb:00/00/00
xterm*color0: rgb:00/00/00
xterm*color1: rgb:a8/00/00
xterm*color2: rgb:00/a8/00
xterm*color3: rgb:a8/54/00
xterm*color4: rgb:00/00/a8
xterm*color5: rgb:a8/00/a8
xterm*color6: rgb:00/a8/a8
xterm*color7: rgb:a8/a8/a8
xterm*color8: rgb:54/54/54
xterm*color9: rgb:fc/54/54
xterm*color10: rgb:54/fc/54
xterm*color11: rgb:fc/fc/54
xterm*color12: rgb:54/54/fc
xterm*color13: rgb:fc/54/fc
xterm*color14: rgb:54/fc/fc
xterm*color15: rgb:fc/fc/fc

! right hand side scrollbar...
xterm*rightScrollBar: true
xterm*ScrollBar: true

! stop output to terminal from jumping down to bottom of scroll again
xterm*scrollTtyOutput: false

!To copy between xterm and other programs/documents/…
!SHIFT+INSERT = PASTE
!SHIFT+PRINTSCREEN = COPY
XTerm*selectToClipboard: true
```

# Notes:

Ctrl-Alt-F2 to switch console to tty1

Alt-F9 go back to X

https://www.freshports.org/graphics/intel-backlight



# misc

`sudo pw group mod video -m user23`

For Linux® compatibility to be enabled at boot time, add this line to /etc/rc.conf:
`linux_enable="YES"`
or at runtime
```sh
kldload linux64
kldload linux
```

```sh
pkg install git xorg-server xorg-fonts-truetype gcr devel/glib20 xorg-fonts-type1 p5-X11-Xlib p5-PkgConfig xauth xrandr xinit libXft xrdb webkit2-gtk3 feh xf86-input-mouse xf86-input-keyboard linux-c7-libpng pkgconf ncurses xf86-input-libinput terminus-font
```
